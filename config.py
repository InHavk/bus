import os

__author__ = 'yozis'


BASE_PATH = os.path.dirname(__file__)
LOG_FILE = os.path.join(BASE_PATH, 'logs', 'log.txt')
DATABASE = {
    'db_name': 'bus',
    'user': 'bus',
    'pass': '123456',
    'host': 'localhost'
}
DEBUG = True
SECRET_KEY = 'development key'
