import argparse
import db
import os
import sys
from config import DATABASE
from parser import TrackParser, StationParser, TrackSeparator


def handle_db(args_: argparse.Namespace):
    if args_.command == 'init':
        db.init_tables()
    elif args_.command == 'info':
        conn = db.get_conn()
        cursor = db.get_cursor(conn)
        cursor.execute('SELECT pg_size_pretty( pg_database_size( %s ) )', (DATABASE['db_name'], ))
        db_size = cursor.fetchone()
        print('db_size {}'.format(db_size[0]))


def handle_parser(args_: argparse.Namespace):
    if args_.parser == 'track':
        TrackParser.parse_file(path=args_.file)
    elif args_.parser == 'station':
        StationParser.parse_file(path=args_.file)
    elif args_.parser == 'separator':
        TrackSeparator.sep_file(path=args_.file)


if __name__ == "__main__":
    sys.path.append(os.path.abspath(os.path.dirname(__file__)))
    parser = argparse.ArgumentParser(prog='BUS STATION PROGRAM')

    parser.add_argument('--foo', action='store_true', help='foo help')
    subparsers = parser.add_subparsers(help='sub-command help')

    parser_db = subparsers.add_parser('db', help='Some commands for db such `init` or `info`')
    parser_db.add_argument('command', type=str, help='Init DB', default=False, choices=('init', 'info'))
    parser_db.set_defaults(handler=handle_db)

    parser_parser = subparsers.add_parser('parse', help='Handle files: parse, separate or something else')
    parser_parser.add_argument('parser', type=str, help='Kind of parser: `track`, `station` or `separator`',
                               choices=('track', 'station', 'separator'))
    parser_parser.add_argument('file', type=str, help='Parse file')

    parser_parser.set_defaults(handler=handle_parser)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit()  # INFO: because you should provider one of subparsers argument
    else:
        args = parser.parse_args()
        args.handler(args)
