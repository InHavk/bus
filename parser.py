import functools
import logging
import logging.handlers
import os
import psycopg2
import re
from config import BASE_PATH, LOG_FILE
from datetime import datetime
from db import conn, get_cursor
from urllib.parse import urlparse, parse_qs

rh = logging.handlers.RotatingFileHandler(filename=LOG_FILE, )

logging.basicConfig(level=logging.DEBUG, handlers=[rh],
                    format='%(levelname)s %(filename)s[LINE:%(lineno)d]  [%(asctime)s]  %(message)s')


def progress_bar(progress, mark=''):
    print('\r[{0}] {1}%'.format(mark, round(progress, 3)), end='')


def recursive_walker(func):
    def walker(path, callback, args, kwargs):
        if os.path.isdir(path):
            for dirname, dirnames, filenames in os.walk(path):
                for subdirname in dirnames:
                    walker(os.path.join(dirname, subdirname), callback, args, kwargs)
                for filename in filenames:
                    walker(os.path.join(dirname, filename), callback, args, kwargs)
        else:
            callback(path=path, *args, **kwargs)

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if 'path' not in kwargs:
            raise Exception('Pls setup "path" for scan as keyword argument')
        walker(kwargs.pop('path'), func, args, kwargs)

    return wrapper


class TrackParser(object):
    @classmethod
    @recursive_walker
    def parse_file(cls, path):
        """
        :type path: str
        :return:
        """
        print()
        logging.info('Try get n string')
        with open(path, 'r') as f_:
            strings_n = sum(1 for l in f_)
            logging.info('File {file_name} have {n} string'.format(file_name=f_.name, n=strings_n))

        with open(path, 'r') as f_:
            logging.info('parse file {file_name}'.format(file_name=f_.name))
            cur = get_cursor(conn)
            for i, line in enumerate(f_):
                record = cls.parse_line(line)
                if record:
                    try:
                        record['mark'] = os.path.basename(f_.name)
                        cur.execute('''INSERT INTO track(avg_speed, direction, route, time, uuid, location, mark)
                                VALUES (%(avg_speed)s, %(direction)s, %(route)s, %(time)s, %(uuid)s,
                                  ST_GeographyFromText('SRID=4326;POINT(%(latitude)s %(longitude)s)'), %(mark)s)''',
                                    {
                                        'avg_speed': float(record['avg_speed']),
                                        'direction': int(float(record['direction'])),
                                        'route': record['route'],
                                        'time': record['time'],
                                        'uuid': record['uuid'],
                                        'latitude': float(record['latitude']),
                                        'longitude': float(record['longitude']),
                                        'mark': record['mark'],
                                    })
                    except psycopg2.IntegrityError as e:
                        conn.commit()
                        logging.warning('in line {line_n} -  {error}'.format(line_n=i, error=e))

                    if i % 100:
                        conn.commit()
                        progress_bar(i * 100 / strings_n, os.path.basename(f_.name))
                    elif i == strings_n - 1:
                        progress_bar(100, os.path.basename(f_.name))
            conn.commit()


    @classmethod
    def parse_line(cls, line):
        """
        :type line: str
        :rtype: BusPath|None
        """
        pattern = 'mtprognosis/accept\?avg_speed=(?P<avg_speed>([\d]+)|([\d]+.[\d]+))&category=(?P<category>[\S]+)&clid=(?P<clid>[\S]+)' \
                  '&direction=(?P<direction>([\d]+)|([\d]+.[\d]+))&latitude=(?P<latitude>[\d]+.[\d]+)&longitude=(?P<longitude>[\d]+.[\d]+)' \
                  '&region_id=(?P<region_id>[\d]+)&route=(?P<route>[\d]+)&time=(?P<time>[\d]+%3A[\d]+)&uuid=(?P<uuid>[\S]+)' \
                  '&vehicle_type=(?P<vehicle_type>trolleybus|bus|tramway|minibus)'

        pattern = 'mtprognosis/accept\?([\S]+) HTTP/1.1'
        p = re.compile(pattern)
        result = p.search(line)
        if result:
            data = parse_qs(p.search(line).group(1))
            data = {param: data[param][0] for param in data}
            required_fields = (
                'avg_speed', 'direction', 'route', 'time', 'uuid', 'latitude', 'longitude', 'vehicle_type')

            if set(required_fields).issubset(set(data.keys())) and data['vehicle_type'] == 'bus':
                # все поля присутствуют
                data['time'] = datetime.strptime(data['time'], r'%d%m%Y:%H%M%S')

                return data
            elif 'vehicle_type' in data and data['vehicle_type'] != 'bus':
                return None

        logging.warning('error while parse line: {line}'.format(line=line))
        return None


class StationParser(object):
    @classmethod
    @recursive_walker
    def parse_file(cls, path):
        """
        :type path: str
        :return:
        """

        with open(path, 'r') as f_:
            logging.info('parse file {file_name}'.format(file_name=f_.name))
            cur = get_cursor(conn)

            for i, line in enumerate(f_):
                record = cls.parse_line(line)

                # print('parsing line #{i}'.format(i=i))
                if record:
                    try:
                        record['mark'] = os.path.basename(f_.name)
                        record['direction'] = None if 'direction' not in record else record['direction']
                        cur.execute(
                            '''INSERT INTO station(name, mark, location, uuid)
                        VALUES (%(name)s, %(mark)s,
                          ST_GeographyFromText('SRID=4326;POINT(%(latitude)s %(longitude)s)'), %(uuid)s)''',
                            {
                                'name': '{name} [{lat}, {lon}]'.format(name=record['name'], lat=record['latitude'],
                                                                       lon=record['longitude']),
                                'mark': record['mark'],
                                'latitude': float(record['latitude']),
                                'longitude': float(record['longitude']),
                                'uuid': '{mark}_{dir}_{lat}_{lon}'.format(mark=record['mark'], lat=record['latitude'],
                                                                          lon=record['longitude'],
                                                                          dir=record['direction']),
                            })
                    except psycopg2.IntegrityError as e:
                        conn.commit()
                        logging.warning(e)
                else:
                    logging.warning('error while parse line: {line}'.format(line=line))
            conn.commit()


    @classmethod
    def parse_line(cls, line):
        """
        :type line: str
        """
        pattern = '(?P<name>[\S\s]+)[\s]+(?P<latitude>[\d]+.[\d]+)[\s]+(?P<longitude>[\d]+.[\d]+)'
        p = re.compile(pattern)
        result = p.search(line)
        if result:
            data = result.groupdict()
            record = data

        else:
            record = None

        return record


class TrackSeparator(object):
    """
    Делит файл логов по номерам автобусов
    """

    @classmethod
    @recursive_walker
    def sep_file(cls, path):
        """
        :type path: str
        :return:
        """
        print()
        logging.info('Try get n string')
        with open(path, 'r') as f_:
            strings_n = sum(1 for l in f_)
            logging.info('File {file_name} have {n} string'.format(file_name=f_.name, n=strings_n))

        routes_inods = dict()

        with open(path, 'r') as f_:
            logging.info('separate file {file_name}'.format(file_name=f_.name))
            for i, line in enumerate(f_):
                pattern = 'route=([\d]+)'
                p = re.compile(pattern)
                result = p.findall(line)
                if result:
                    route = result[0]
                    if route not in routes_inods:
                        routes_inods[route] = open(os.path.join(BASE_PATH, 'data', 'sep_tracks',
                                                                '{f_name}.route_{route}'.format(
                                                                    f_name=os.path.basename(f_.name),
                                                                    route=route)), 'w')
                    routes_inods[route].write(line)
                if i % 100:
                    progress_bar(i * 100 / strings_n, os.path.basename(f_.name))
                elif i == strings_n - 1:
                    progress_bar(100, os.path.basename(f_.name))
        for route_ in routes_inods:
            routes_inods[route_].close()


if __name__ == '__main__':
    StationParser.parse_file(path='data/bus_stops/6route.tsv')