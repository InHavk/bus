import numpy as np
from sklearn.cluster import MeanShift, estimate_bandwidth
from time import time
import db
import json

__author__ = 'Anton Trusov <inhavk@gmail.com>'

ROUTE = "023"

conn = db.conn

gps_coords = []

cursor = db.get_cursor(conn)
cursor.execute('''SELECT ST_X(track.location::GEOMETRY), ST_Y(track.location::GEOMETRY) FROM track
                  WHERE route=%(route)s AND avg_speed=0''',
              {'route': ROUTE})

gps_coords = [[track_id[0], track_id[1]] for track_id in cursor]


gps_coords = np.array(gps_coords)
bandwidth = estimate_bandwidth(gps_coords, quantile=0.02)
ms = MeanShift(bandwidth=bandwidth, bin_seeding=True)
ms.fit(gps_coords)

cluster_centers = ms.cluster_centers_
n_clusters_ = len(np.unique(ms.labels_))

print(n_clusters_)

out = {"type": "FeatureCollection", "features": []}

for i, k in zip(range(n_clusters_), cluster_centers):
    geometry = {"type": "Point", "coordinates": [k[0], k[1]]}
    properties = {"clusterCaption": "one more", "hintContent": ""}
    record = {"type": "Feature", "id": i, "geometry": geometry, "properties": properties}
    out["features"].append(record)

out = json.dumps(out, sort_keys=True)
print(out)