var initMapCallback = function () {
    var center = this.center;
    var heatmapData = this.heatmapData;
    var placemarks = this.placemarks;

    var map = new ymaps.Map('YMapsID', {
            center: center,
            controls: ['zoomControl', 'typeSelector', 'fullscreenControl'],
            zoom: 12, type: 'yandex#satellite'
        }),

        buttons = {
            dissipating: new ymaps.control.Button({
                data: {
                    content: 'Toggle dissipating'
                },
                options: {
                    selectOnClick: false,
                    maxWidth: 150
                }
            }),
            opacity: new ymaps.control.Button({
                data: {
                    content: 'Change opacity'
                },
                options: {
                    selectOnClick: false,
                    maxWidth: 150
                }
            }),
            radius: new ymaps.control.Button({
                data: {
                    content: 'Change radius'
                },
                options: {
                    selectOnClick: false,
                    maxWidth: 150
                }
            }),
            gradient: new ymaps.control.Button({
                data: {
                    content: 'Reverse gradient'
                },
                options: {
                    selectOnClick: false,
                    maxWidth: 150
                }
            }),
            heatmap: new ymaps.control.Button({
                data: {
                    content: 'Toggle Heatmap'
                },
                options: {
                    selectOnClick: false,
                    maxWidth: 150
                }
            })
        },

        gradients = [{
            0.1: 'rgba(128, 255, 0, 0.7)',
            0.2: 'rgba(255, 255, 0, 0.8)',
            0.7: 'rgba(234, 72, 58, 0.9)',
            1.0: 'rgba(162, 36, 25, 1)'
        }, {
            0.1: 'rgba(162, 36, 25, 0.7)',
            0.2: 'rgba(234, 72, 58, 0.8)',
            0.7: 'rgba(255, 255, 0, 0.9)',
            1.0: 'rgba(128, 255, 0, 1)'
        }],
        radiuses = [5, 10, 20, 30],
        opacities = [0.4, 0.6, 0.8, 1];

    ymaps.modules.require(['Heatmap'], function (Heatmap) {
        var heatmap = new Heatmap(heatmapData, {
            gradient: gradients[0],
            radius: radiuses[1],
            opacity: opacities[2]
        });
        heatmap.setMap(map);

        buttons.dissipating.events.add('press', function () {
            heatmap.options.set(
                'dissipating', !heatmap.options.get('dissipating')
            );
        });
        buttons.opacity.events.add('press', function () {
            var current = heatmap.options.get('opacity'),
                index = opacities.indexOf(current);
            heatmap.options.set(
                'opacity', opacities[++index == opacities.length ? 0 : index]
            );
        });
        buttons.radius.events.add('press', function () {
            var current = heatmap.options.get('radius'),
                index = radiuses.indexOf(current);
            heatmap.options.set(
                'radius', radiuses[++index == radiuses.length ? 0 : index]
            );
        });
        buttons.gradient.events.add('press', function () {
            var current = heatmap.options.get('gradient');
            heatmap.options.set(
                'gradient', current == gradients[0] ? gradients[1] : gradients[0]
            );
        });
        buttons.heatmap.events.add('press', function () {
            heatmap.setMap(
                heatmap.getMap() ? null : map
            );
        });

        for (var key in buttons) {
            if (buttons.hasOwnProperty(key)) {
                map.controls.add(buttons[key]);
            }
        }
    });


    placemarks.forEach(function (placemark) {
        var coor = [placemark[1], placemark[2]];
        var id = placemark[0];
        var myPlacemark = new ymaps.Placemark(coor, {
            hintContent: 'id#'+id,
            balloonContent: '['+coor[0]+','+coor[1]+']'
        });
        map.geoObjects.add(myPlacemark);

    });
};


var HeatMap = {
    initMap: function (center, heatmapData, placemarks) {
        ymaps.ready(initMapCallback, {center: center, heatmapData: heatmapData, placemarks: placemarks});
    }
};
