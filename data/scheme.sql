--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: btree_gin; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS btree_gin WITH SCHEMA public;


--
-- Name: EXTENSION btree_gin; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION btree_gin IS 'support for indexing common datatypes in GIN';


--
-- Name: btree_gist; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS btree_gist WITH SCHEMA public;


--
-- Name: EXTENSION btree_gist; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION btree_gist IS 'support for indexing common datatypes in GiST';


--
-- Name: postgis; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS postgis WITH SCHEMA public;


--
-- Name: EXTENSION postgis; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION postgis IS 'PostGIS geometry, geography, and raster spatial types and functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: station; Type: TABLE; Schema: public; Owner: bus; Tablespace: 
--

CREATE TABLE station (
    id integer NOT NULL,
    name character varying NOT NULL,
    mark character varying NOT NULL,
    location geography(Point,4326) NOT NULL,
    uuid character varying NOT NULL,
    bool_direction boolean
);


ALTER TABLE public.station OWNER TO bus;

--
-- Name: COLUMN station.name; Type: COMMENT; Schema: public; Owner: bus
--

COMMENT ON COLUMN station.name IS 'Название остановки';


--
-- Name: COLUMN station.mark; Type: COMMENT; Schema: public; Owner: bus
--

COMMENT ON COLUMN station.mark IS 'Метка этого набора данных, скажем рейса';


--
-- Name: COLUMN station.location; Type: COMMENT; Schema: public; Owner: bus
--

COMMENT ON COLUMN station.location IS 'Координаты станции';


--
-- Name: COLUMN station.uuid; Type: COMMENT; Schema: public; Owner: bus
--

COMMENT ON COLUMN station.uuid IS 'Колонка для ограничения уникального ключа формата: mark_direction_lat_lon

Postgres содержит баг с индексами по geography, эта колонка - workaround';


--
-- Name: station_id_seq; Type: SEQUENCE; Schema: public; Owner: bus
--

CREATE SEQUENCE station_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.station_id_seq OWNER TO bus;

--
-- Name: station_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bus
--

ALTER SEQUENCE station_id_seq OWNED BY station.id;


--
-- Name: track; Type: TABLE; Schema: public; Owner: bus; Tablespace: 
--

CREATE TABLE track (
    id integer NOT NULL,
    avg_speed real DEFAULT 0 NOT NULL,
    direction integer DEFAULT 0 NOT NULL,
    route character varying NOT NULL,
    "time" timestamp without time zone NOT NULL,
    location geography(Point,4326) NOT NULL,
    mark character varying,
    uuid character varying NOT NULL,
    from_start boolean
);


ALTER TABLE public.track OWNER TO bus;

--
-- Name: TABLE track; Type: COMMENT; Schema: public; Owner: bus
--

COMMENT ON TABLE track IS 'Точки треков';


--
-- Name: COLUMN track.avg_speed; Type: COMMENT; Schema: public; Owner: bus
--

COMMENT ON COLUMN track.avg_speed IS 'Средняя скорость';


--
-- Name: COLUMN track.route; Type: COMMENT; Schema: public; Owner: bus
--

COMMENT ON COLUMN track.route IS 'Маршрут';


--
-- Name: COLUMN track."time"; Type: COMMENT; Schema: public; Owner: bus
--

COMMENT ON COLUMN track."time" IS 'Время  точки';


--
-- Name: COLUMN track.location; Type: COMMENT; Schema: public; Owner: bus
--

COMMENT ON COLUMN track.location IS 'Координаты станции';


--
-- Name: COLUMN track.mark; Type: COMMENT; Schema: public; Owner: bus
--

COMMENT ON COLUMN track.mark IS 'Метка источника записи. Может использоватсья для ограничения набора точек и станций';


--
-- Name: track_id_seq; Type: SEQUENCE; Schema: public; Owner: bus
--

CREATE SEQUENCE track_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.track_id_seq OWNER TO bus;

--
-- Name: track_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bus
--

ALTER SEQUENCE track_id_seq OWNED BY track.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bus
--

ALTER TABLE ONLY station ALTER COLUMN id SET DEFAULT nextval('station_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: bus
--

ALTER TABLE ONLY track ALTER COLUMN id SET DEFAULT nextval('track_id_seq'::regclass);


--
-- Name: pk_bus_station_id; Type: CONSTRAINT; Schema: public; Owner: bus; Tablespace: 
--

ALTER TABLE ONLY station
    ADD CONSTRAINT pk_bus_station_id PRIMARY KEY (id);


--
-- Name: pk_bus_track_id; Type: CONSTRAINT; Schema: public; Owner: bus; Tablespace: 
--

ALTER TABLE ONLY track
    ADD CONSTRAINT pk_bus_track_id PRIMARY KEY (id);


--
-- Name: uc_bus_track_mark_uuid_time; Type: CONSTRAINT; Schema: public; Owner: bus; Tablespace: 
--

ALTER TABLE ONLY track
    ADD CONSTRAINT uc_bus_track_mark_uuid_time UNIQUE (mark, uuid, "time");


--
-- Name: CONSTRAINT uc_bus_track_mark_uuid_time ON track; Type: COMMENT; Schema: public; Owner: bus
--

COMMENT ON CONSTRAINT uc_bus_track_mark_uuid_time ON track IS 'В одном файле одно ТС в единицу времени дает лишь 1 координату';


--
-- Name: uc_uuid; Type: CONSTRAINT; Schema: public; Owner: bus; Tablespace: 
--

ALTER TABLE ONLY station
    ADD CONSTRAINT uc_uuid UNIQUE (uuid);


--
-- Name: i_bus_station_location; Type: INDEX; Schema: public; Owner: bus; Tablespace: 
--

CREATE INDEX i_bus_station_location ON station USING gist (location);


--
-- Name: i_bus_station_mark; Type: INDEX; Schema: public; Owner: bus; Tablespace: 
--

CREATE INDEX i_bus_station_mark ON station USING btree (mark);


--
-- Name: i_bus_station_name; Type: INDEX; Schema: public; Owner: bus; Tablespace: 
--

CREATE INDEX i_bus_station_name ON station USING btree (name);


--
-- Name: i_bus_track_avg_speed; Type: INDEX; Schema: public; Owner: bus; Tablespace: 
--

CREATE INDEX i_bus_track_avg_speed ON track USING gin (avg_speed);


--
-- Name: i_bus_track_direction; Type: INDEX; Schema: public; Owner: bus; Tablespace: 
--

CREATE INDEX i_bus_track_direction ON track USING gin (direction);


--
-- Name: i_bus_track_location; Type: INDEX; Schema: public; Owner: bus; Tablespace: 
--

CREATE INDEX i_bus_track_location ON track USING gist (location);


--
-- Name: i_bus_track_mark; Type: INDEX; Schema: public; Owner: bus; Tablespace: 
--

CREATE INDEX i_bus_track_mark ON track USING btree (mark);


--
-- Name: i_bus_track_route; Type: INDEX; Schema: public; Owner: bus; Tablespace: 
--

CREATE INDEX i_bus_track_route ON track USING btree (route);


--
-- Name: i_bus_track_time; Type: INDEX; Schema: public; Owner: bus; Tablespace: 
--

CREATE INDEX i_bus_track_time ON track USING gin ("time");


--
-- Name: i_bus_track_uuid; Type: INDEX; Schema: public; Owner: bus; Tablespace: 
--

CREATE INDEX i_bus_track_uuid ON track USING btree (uuid);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

