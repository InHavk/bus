from datetime import datetime
import json
import sys
import os
import psycopg2
from config import DATABASE

sys.path.append(os.path.dirname(__file__))
from flask import Flask, render_template, g

app = Flask(__name__)
app.config.from_object('config')


@app.teardown_appcontext
def close_connection(exception):
    conn_ = getattr(g, '_database', None)
    if conn_ is not None:
        conn_.close()


def get_conn() -> psycopg2.extensions.connection:
    conn_ = getattr(g, '_database', None)
    if conn_ is None:
        conn_ = g._database = psycopg2.connect(database=DATABASE['db_name'], user=DATABASE['user'],
                                               password=DATABASE['pass'], host=DATABASE['host'])
    return conn_


def get_cursor(conn_) -> psycopg2.extensions.cursor:
    return conn_.cursor()


@app.route('/')
def hello_world():
    return 'Hello World!'


@app.route('/heat')
def heat_map():
    conn = get_conn()
    cursor = get_cursor(conn)
    cursor.execute('''SELECT id, ST_X(location::GEOMETRY), ST_Y(location::GEOMETRY)
                      FROM STATION WHERE mark='36route.tsv' ORDER BY id;''')
    stations = ((1, 55.556708, 38.620766), (2, 55.565171, 38.227452))

    points = []

    cursor.execute(
        '''SELECT ST_X(location::GEOMETRY), ST_Y(location::GEOMETRY) FROM track
           WHERE route='36' AND uuid='4980194_1056' AND time>%(time1)s AND time<%(time2)s''',
        {
            'r': int(100),  # 100 метров,
            'time1': datetime(2015, 4, 12, 9, 19, 41),
            'time2': datetime(2015, 4, 12, 10, 35, 41),
        }
    )
    station_points = cursor.fetchall()
    points += station_points

    return render_template('heat.html', points=json.dumps(points), stations=json.dumps(stations))


@app.route('/time')
def time_map():
    conn = get_conn()
    cursor = get_cursor(conn)
    cursor.execute('SELECT id, ST_X(location::GEOMETRY), ST_Y(location::GEOMETRY) FROM STATION ORDER BY id;')
    stations = cursor.fetchall()

    points = []
    for id_, latitude, longitude in stations:
        print('station #{id_}'.format(id_=id_))

        cursor.execute(
            '''SELECT ST_X(location::geometry), ST_Y(location::geometry), time FROM track
               WHERE ST_DWithin(location, ST_GeographyFromText('SRID=4326;POINT(%(latitude)s %(longitude)s)'), %(r)s)''',
            {
                'latitude': float(latitude),
                'longitude': float(longitude),
                'r': int(100)  # 100 метров
            }
        )
        station_points = cursor.fetchall()
        points += station_points

    return render_template('time.html', points=json.dumps(points), stations=json.dumps(stations))


if __name__ == '__main__':
    app.run(debug=True)
