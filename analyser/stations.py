from datetime import datetime
import itertools
from analyser.schedule import Forel
import db

STATION_RADIUS = 100
conn = db.conn

'''
ALTER TABLE public.track ADD bool_direction BOOLEAN DEFAULT NULL  NULL;
'''


class Stations(object):
    @staticmethod
    def mark_point_direction(route, terminal_stations):
        """
        Помечает точки трека как идущие "туда" или "обратно"
        Для этого нужны координаты конечных станций и маршрут рейса
        :return:
        """
        cursor = db.get_cursor(conn)
        start_station, finish_station = terminal_stations

        cursor.execute('''SELECT uuid FROM track
                          WHERE route=%(route)s AND
                          ST_DWithin(
                            location, ST_GeographyFromText('SRID=4326;POINT(%(lat)s %(lon)s)'), %(r)s
                          ) GROUP BY uuid
                        ''', {'route': route, 'r': 100, 'lat': start_station[0], 'lon': start_station[1]})
        uuids = [row[0] for row in cursor]
        for uuid in uuids:
            cursor.execute(
                '''SELECT track.id FROM track
                    WHERE ST_DWithin(track.location,
                      ST_GeographyFromText('SRID=4326;POINT(%(lat)s %(lon)s)'),  %(r)s
                    ) AND route=%(route)s AND uuid=%(uuid)s''',
                {'r': 200, 'lat': start_station[0], 'lon': start_station[1], 'route': route, 'uuid': uuid})
            start_track_ids = [track_id[0] for track_id in cursor]

            cursor.execute(
                '''SELECT track.id FROM track
                    WHERE ST_DWithin(track.location,
                      ST_GeographyFromText('SRID=4326;POINT(%(lat)s %(lon)s)'),  %(r)s
                    ) AND route=%(route)s AND uuid=%(uuid)s''',
                {'r': 200, 'lat': finish_station[0], 'lon': finish_station[1], 'route': route, 'uuid': uuid})
            finish_track_ids = [track_id[0] for track_id in cursor]

            start_schedule = Forel.clustering_station_tracks(start_track_ids, 60 * 60)
            finish_schedule = Forel.clustering_station_tracks(finish_track_ids, 60 * 60)
            start_schedule.sort(key=lambda i: i['centroid_time'])
            finish_schedule.sort(key=lambda i: i['centroid_time'])

            times = [(time['centroid_time'], True) for time in start_schedule]
            times += [(time['centroid_time'], False) for time in finish_schedule]
            times.sort(key=lambda t_: t_[0])

            """
                Маркируем только перемещения от одной конечки до другой, в противном случае это был не рейс
                Также не маркируем перемещения во время обеденного перерыва (когда дважды подряд посещена та же конечка)
            """
            while len(times) >= 2:
                time, from_start = times.pop(0)
                second_time, second_direction = times[0]
                if from_start != second_direction:
                    cursor.execute('''UPDATE track SET from_start = %(from_start)s
                                      WHERE route=%(route)s AND uuid=%(uuid)s AND time BETWEEN %(start_time)s AND %(end_time)s
                                    ''',
                                   {
                                       'from_start': from_start,
                                       'route': route,
                                       'uuid': uuid,
                                       'end_time': second_time,
                                       'start_time': time,
                                   })
                    conn.commit()
                else:
                    continue
        pass


if __name__ == '__main__':
    # остановки: 49км - Автовокзал раменское (49км не обозначен в яндекс расписаниях)
    # https://rasp.yandex.ru/thread/36B_f9736892t9736654_mta?tt=bus
    Stations.mark_point_direction('36', ((55.556708, 38.620766), (55.565171, 38.227452)))

