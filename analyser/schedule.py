from datetime import datetime
import db

__author__ = 'yozis'

SCHEDULE_STATION_RADIUS = 50
SCHEDULE_TIME_INTERVAL = 60 * 20

conn = db.conn


class Forel(object):
    @classmethod
    def get_schedule(cls, station_coords, route):
        stations_schedules = {}
        for station_coord in station_coords:
            stations_schedules[station_coord] = cls.solve_station_schedule(station_coord, route)
            pass
        pass
        return stations_schedules

    @classmethod
    def solve_station_schedule(cls, station_coord, route):
        cursor = db.get_cursor(conn)

        # возмем точки, ассоциированные с остановкой
        cursor.execute(
            '''SELECT track.id FROM track
                WHERE ST_DWithin(track.location,
                  ST_GeographyFromText('SRID=4326;POINT(%(lat)s %(lon)s)'),  %(r)s
                ) AND route=%(route)s''',
            {'r': SCHEDULE_STATION_RADIUS, 'lat': station_coord[0], 'lon': station_coord[1], 'route': route})
        station_tracks_ids = cursor.fetchall()
        station_tracks_ids = [station_track_id[0] for station_track_id in station_tracks_ids]

        return cls.clustering_station_tracks(station_tracks_ids)

    @staticmethod
    def clustering_station_tracks(station_tracks_ids, time_interval=None):
        if time_interval is None:
            time_interval = SCHEDULE_TIME_INTERVAL
        cursor = db.get_cursor(conn)

        clustered_points = []  # точки, которые уже в кластерах
        centroids = []  # центры кластеров, в данном случае время остановок

        while len(clustered_points) != len(station_tracks_ids):
            # выберем случайную точку, как центр кластера
            cursor.execute('''SELECT time FROM track WHERE id = ANY(%s) AND NOT id = ANY(%s) LIMIT 1''',
                           (station_tracks_ids, clustered_points))
            centroid_time = cursor.fetchone()[0]
            cluster_points = station_tracks_ids
            old_centroid_time = None
            while centroid_time != old_centroid_time:
                old_centroid_time = centroid_time
                # возьмем точки, ассоциированные с кластером
                cursor.execute('''SELECT id FROM track
                                  WHERE id = ANY(%(tracks_ids)s) AND NOT id=ANY(%(clustered_points)s)
                                  AND time BETWEEN %(time_l)s AND %(time_r)s''',
                               {'tracks_ids': cluster_points,
                                'time_l': datetime.fromtimestamp(centroid_time.timestamp() - time_interval),
                                'time_r': datetime.fromtimestamp(centroid_time.timestamp() + time_interval),
                                'clustered_points': clustered_points,
                               })
                cluster_points = cursor.fetchall()
                cluster_points = [track_id[0] for track_id in cluster_points]

                # вычислим новый центр кластера
                cursor.execute('''SELECT to_timestamp(AVG(extract(EPOCH FROM  time))) AT TIME ZONE 'utc' FROM track
                                  WHERE id = ANY (%s)''',
                               (cluster_points, ))
                centroid_time = cursor.fetchone()[0]
                pass
            else:
                clustered_points = list(set(clustered_points).union(set(cluster_points)))
                centroids.append({'centroid_time': centroid_time, 'points_count': len(cluster_points)})
            pass

        return centroids


if __name__ == '__main':
    cursor_ = db.get_cursor(conn)
    cursor_.execute('SELECT  ST_X(location::GEOMETRY), ST_Y(location::GEOMETRY) FROM station WHERE mark=%(mark)s',
                    {'mark': '36route.tsv'})
    station_coords_ = cursor_.fetchall()
    Forel.get_schedule(station_coords_, '36')